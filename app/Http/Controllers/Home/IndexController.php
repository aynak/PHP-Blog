<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Http\Model\Article;
use App\Http\Model\Category;
use App\Http\Model\Links;
use Illuminate\Http\Request;

class IndexController extends CommonController
{
    public function index()
    {
        //点击量最高的6篇文章
        $pics = Article::orderBy('art_view','desc')->take(6)->get();

        //图文列表5篇文章（带分页）
        $data = Article::orderBy('art_time','desc')->paginate(5);

        //获取友情链接
        $links = Links::orderBy('link_order','asc')->get();

        return view('home.index',compact('pics','data','links'));
    }

    public function cate($cate_id)
    {
        $field = Category::find($cate_id);
        //图文列表5篇文章（带分页）
        $data = Article::where('cate_id',$cate_id)->orderBy('art_time','desc')->paginate(5);
        $submenu = Category::where('cate_pid',$cate_id)->get();
        //查看次数的增长
        Category::where('cate_id',$cate_id)->increment('cate_view');
        return view('home.list',compact('field','data','submenu'));
    }

    public function article($art_id)
    {
        $data = Article::Join('category','article.cate_id','=','category.cate_id')->where('art_id',$art_id)->first();
        $article['pre'] = Article::where('art_id','<',$art_id)->orderBy('art_id','desc')->first();
        $article['next'] = Article::where('art_id','>',$art_id)->orderBy('art_id','asc')->first();
        $relates = Article::where('cate_id',$data->cate_id)->orderBy('art_id','desc')->take(6)->get();
        //查看次数的增长
        Article::where('art_id',$art_id)->increment('art_view');
        return view('home.new',compact('data','article','relates'));
    }
}
