<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\conf;
use function Sodium\library_version_major;

class ConfigController extends CommonController
{
    public function index()
    {
        $data = Config::orderBy('conf_order','asc')->get();
        foreach($data as $k=>$v){
            switch($v->field_type){
                case 'input':
                    $data[$k]->_html = '<input type="text" class="lg" name="conf_content[]" value="'.$v->conf_content.'">';
                    break;
                case 'textarea':
                    $data[$k]->_html = '<textarea class="lg" name="conf_content[]">'.$v->conf_content.'</textarea>';
                    break;
                case 'radio':
                    $arr = explode(',',$v->field_value);
                    $str = '';
                    foreach($arr as $m=>$n){
                        $r = explode('|',$n);
                        $c = $v->conf_content==$r[0]?' checked ':'';
//                        $c = '';
//                        if($v->conf_content==$r[0]){
//                            $c = ' checked ';
//                        }
                        $str.='<input type="radio" name="conf_content[]" value="'.$r[0].'"'.$c.'>'.$r[1].'　　';
                    }
                    $data[$k]->_html = $str;
                    break;
            }
        }
        return view('admin.config.index',compact('data'));
    }

    public function putFile()
    {
        $message = Config::pluck('conf_content','conf_title')->all();
        $path = base_path().'\config\web.php';
        $str = '<?php return '.var_export($message,true).';';
        file_put_contents($path,$str);
    }

    public function changeContent(Request $request)
    {
        $input = $request->all();
        foreach($input['conf_id'] as $k=>$v){
            Config::where('conf_id',$v)->update(['conf_content'=>$input['conf_content'][$k]]);
        }
        $this->putFile();
        return back()->with('errors',"配置项更新成功！");
    }

    public function changeOrder(Request $request)
    {
        $input = $request->input();
        $conf = Config::find($input['conf_id']);
        $conf->conf_order = $input['conf_order'];
        $re = $conf->update();
        if($re){
            $data = [
                'status'=>0,
                'msg'=>'配置项排序更新成功,请您刷新页面！'
            ];
        }else{
            $data = [
                'status'=>1,
                'msg'=>'配置项排序更新失败，请稍后再试试！'
            ];
        }
        return $data;
    }

    //  admin/config/create  添加配置项
    public function create()
    {
        return view('admin/config/add');
    }

    //admin/config/store  添加配置项提交方法
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $rules = [
            'conf_name'=>'required',
            'conf_title'=>'required',
        ];
        $message = [
            'conf_name.required'=>'配置项名称是必须要填写！',
            'conf_title.required'=>'配置项标题是必须要填写！',
        ];
        $validator = Validator::make($input,$rules,$message);
        if($validator->passes()){
            $re = Config::create($input);
            if($re){
                return redirect('admin/config');
            }else{
                return back()->with('errors',"配置项填写失败，请稍后再重试！");
            }
        }else{
            return back()->withErrors($validator);
        }
    }

    //  admin/config/{conf_id}/edit 编辑配置项
    public function edit($conf_id)
    {
        $field = Config::find($conf_id);
        return view('admin.config.edit',compact('field'));
    }

    //  admin/config/{conf_id}   更新链接修改数据
    public function update(Request $request,$conf_id)
    {
        $input = $request->except('_token','_method');
        $re = Config::where('conf_id',$conf_id)->update($input);
        if($re){
            $this->putFile();
            return redirect('admin/config');
        }else{
            return back()->with('errors',"数据更新失败，请稍后再重试！");
        }
    }

    // delete  admin/conf/{conf_id}  删除配置项
    public function destroy($conf_id)
    {
        $re = Config::where('conf_id',$conf_id)->delete();
        if($re){
            $this->putFile();
            $data = [
                'status'=>0,
                'msg'=>'删除成功！'
            ];
        }else{
            $data = [
                'status'=>1,
                'msg'=>'删除失败，请稍后再试试！'
            ];
        }
        return $data;
    }

}
