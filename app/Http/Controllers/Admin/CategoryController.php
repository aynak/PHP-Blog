<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\Category;
use App\Http\Model\UserTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Array_;
use phpDocumentor\Reflection\Types\Static_;

class CategoryController extends CommonController
{
    //  admin/category   的默认页面
    public function index()
    {
//        $categorys = Category::tree();
        $categorys = (new Category)->tree();
        return view('admin.category.index')->with('data',$categorys);
    }

    public function changeOrder(Request $request)
    {
        $input = $request->input();
        $cate = Category::find($input['cate_id']);
        $cate->cate_order = $input['cate_order'];
        $re = $cate->update();
        if($re){
            $data = [
                'status'=>0,
                'msg'=>'分类排序更新成功,请您刷新页面！'
            ];
        }else{
            $data = [
                'status'=>1,
                'msg'=>'分类排序更新失败，请稍后再试试！'
            ];
        }
        return $data;
    }

    //  admin/category/create  添加分类
    public function create()
    {
        $data = Category::where('cate_pid',0)->get();
        return view('admin/category/add',compact('data'));
    }

    //admin/category/store  添加分类数据提交方法
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $rules = [
            'cate_name'=>'required',
        ];
        $message = [
            'cate_name.required'=>'分类名称是必须要填写！',
        ];
        $validator = Validator::make($input,$rules,$message);
        if($validator->passes()){
            $re = Category::create($input);
            if($re){
                return redirect('admin/category');
            }else{
                return back()->with('errors',"数据填写失败，请稍后再重试！");
            }
        }else{
            return back()->withErrors($validator);
        }
    }

    //  admin/category/{category_id}/edit 编辑分类
    public function edit($cate_id)
    {
        $field = Category::find($cate_id);
        $data = Category::where('cate_pid',0)->get();
        return view('admin.category.edit',compact('field','data'));
    }

    //  admin/category/{category_id}   更新分类修改数据
    public function update(Request $request,$cate_id)
    {
        $input = $request->except('_token','_method');
        $re = Category::where('cate_id',$cate_id)->update($input);
        if($re){
            return redirect('admin/category');
        }else{
            return back()->with('errors',"数据更新失败，请稍后再重试！");
        }
    }

    // delete  admin/category/{category_id}  删除分类
    public function destroy($cate_id)
    {
        $re = Category::where('cate_id',$cate_id)->delete();
        Category::where('cate_pid',$cate_id)->update(['cate_pid'=>0]);
        if($re){
            $data = [
                'status'=>0,
                'msg'=>'删除成功！'
            ];
        }else{
            $data = [
                'status'=>1,
                'msg'=>'删除失败，请稍后再试试！'
            ];
        }
        return $data;
    }


    public function show()
    {

    }

}
