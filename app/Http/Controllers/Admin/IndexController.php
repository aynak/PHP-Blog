<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\UserTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\Caster\PdoCaster;

class IndexController extends CommonController
{
    public function index(){
        return view("admin.index");
    }

    public function info()
    {
        return view('admin.info');
    }
}
