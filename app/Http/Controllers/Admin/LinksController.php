<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Link;

class LinksController extends CommonController
{
    public function index()
    {
        $data = Links::orderBy('link_order','asc')->get();
        return view('admin.links.index',compact('data'));
    }

    public function changeOrder(Request $request)
    {
        $input = $request->input();
        $link = Links::find($input['link_id']);
        $link->link_order = $input['link_order'];
        $re = $link->update();
        if($re){
            $data = [
                'status'=>0,
                'msg'=>'分类排序更新成功,请您刷新页面！'
            ];
        }else{
            $data = [
                'status'=>1,
                'msg'=>'分类排序更新失败，请稍后再试试！'
            ];
        }
        return $data;
    }

    //  admin/links/create  添加友情链接
    public function create()
    {
        return view('admin/links/add');
    }

    //admin/links/store  添加友情链接提交方法
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $rules = [
            'link_name'=>'required',
            'link_url'=>'required',
        ];
        $message = [
            'link_name.required'=>'链接名称是必须要填写！',
            'link_url.required'=>'链接URL是必须要填写！',
        ];
        $validator = Validator::make($input,$rules,$message);
        if($validator->passes()){
            $re = Links::create($input);
            if($re){
                return redirect('admin/links');
            }else{
                return back()->with('errors',"友情链接填写失败，请稍后再重试！");
            }
        }else{
            return back()->withErrors($validator);
        }
    }

    //  admin/links/{link_id}/edit 编辑友情链接
    public function edit($link_id)
    {
        $field = Links::find($link_id);
        return view('admin.links.edit',compact('field'));
    }

    //  admin/links/{link_id}   更新链接修改数据
    public function update(Request $request,$link_id)
    {
        $input = $request->except('_token','_method');
        $re = Links::where('link_id',$link_id)->update($input);
        if($re){
            return redirect('admin/links');
        }else{
            return back()->with('errors',"数据更新失败，请稍后再重试！");
        }
    }

    // delete  admin/link/{link_id}  删除分类
    public function destroy($link_id)
    {
        $re = Links::where('link_id',$link_id)->delete();
        if($re){
            $data = [
                'status'=>0,
                'msg'=>'删除成功！'
            ];
        }else{
            $data = [
                'status'=>1,
                'msg'=>'删除失败，请稍后再试试！'
            ];
        }
        return $data;
    }

}
