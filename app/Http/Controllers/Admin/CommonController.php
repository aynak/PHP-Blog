<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CommonController extends Controller
{
    public function upload(Request $request)
    {
        foreach ($request->file() as $key => $value) {
            if($value->getMimeType()=='image/jpeg' || $value->getMimeType()=='image/png'){
                // $imageName = $value->getClientOriginalName();
                $extension = $value->getClientOriginalExtension();
                $imageName = date('YmdHis').mt_rand(100,999).".".$extension;
                $value->move(base_path().'/uploads', $imageName);
                $file_path = '/uploads/'.$imageName;
            }
        }
        return $data = (['success' => '上传成功!','file_path'=>$file_path]);
    }
}
