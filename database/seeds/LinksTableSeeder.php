<?php

use Illuminate\Database\Seeder;

class LinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'link_name'=>'QQ空间',
                'link_title'=>'分享到QQ空间',
                'link_url'=>'www.qq.com',
                'link_order'=>1
            ],
            [
                'link_name'=>'朋友圈',
                'link_title'=>'分享到微信朋友圈',
                'link_url'=>'www.weixin.com',
                'link_order'=>2
            ]
        ];
        DB::table('links')->insert($data);
    }
}
