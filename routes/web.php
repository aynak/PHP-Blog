<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/','Home\IndexController@index');
Route::get('/cate/{cate_id}','Home\IndexController@cate');
Route::get('/article/{art_id}','Home\IndexController@article');


Route::any('/admin/login', 'Admin\LoginController@login');
Route::get('/admin/code','Admin\LoginController@code');
Route::get('/admin/getcode','Admin\LoginController@getcode');
Route::get('/admin/crypt','Admin\LoginController@crypt');
Route::get('/haha','Admin\LoginController@haha');

Route::group(['middleware'=>['web','admin.login'],'prefix'=>'admin','namespace'=>'Admin'],function(){
    Route::any('/', 'IndexController@index');
    Route::any('info', 'IndexController@info');
    Route::any('quit', 'LoginController@quit');
    Route::any('pass', 'LoginController@pass');
    Route::post('cate/changeorder', 'CategoryController@changeOrder');
    Route::resource('category', 'CategoryController');
    Route::resource('article', 'ArticleController');
    Route::post('upload', 'CommonController@upload');
    Route::resource('links', 'LinksController');
    Route::post('links/changeorder', 'LinksController@changeOrder');
    Route::resource('navs', 'NavsController');
    Route::post('navs/changeorder', 'NavsController@changeOrder');
    Route::resource('config', 'ConfigController');
    Route::post('config/changeorder', 'ConfigController@changeOrder');
    Route::post('config/changecontent', 'ConfigController@changeContent');
});
